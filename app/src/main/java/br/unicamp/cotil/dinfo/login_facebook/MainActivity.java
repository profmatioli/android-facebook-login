package br.unicamp.cotil.dinfo.login_facebook;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;

import org.json.JSONObject;

import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

//48HcZz7h1//Wz376Z24LeVkc370
public class MainActivity extends Activity {
    TextView tvStatus;
    ProfilePictureView imgFotoFB;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "br.unicamp.cotil.matioli.dinfoapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        if (AccessToken.getCurrentAccessToken() == null) {
            goLoginScreen();
        }

        tvStatus = (TextView) findViewById(R.id.tvStatus);
        imgFotoFB = (ProfilePictureView) findViewById(R.id.imgFotoFB);

        getUserDetailsFromFB(AccessToken.getCurrentAccessToken());
    }

    private void goLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void logout(View view) {
        LoginManager.getInstance().logOut();
        goLoginScreen();
    }

    public void getUserDetailsFromFB(AccessToken accessToken) {

        final GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String email =  object.getString("email");
                    String gender = object.getString("gender");
                    String name = object.getString("name");
                    String id = object.getString("id");
                    URL photourl =new URL(object.getJSONObject("picture").getJSONObject("data").getString("url"));

                    imgFotoFB.setProfileId(id);

                    tvStatus.setText("DADOS DO USUÁRIO:\n"+name+"\n"+email+"\n"+id+"\n"+photourl);
                    Log.d("teste",email);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }
}
